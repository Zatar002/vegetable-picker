﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace hw3app1
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FavoriteVegetable : ContentPage
	{
		public FavoriteVegetable ()
		{
			InitializeComponent ();
		}

        public FavoriteVegetable(string name, string veggie)
        {
            InitializeComponent();
            UserEnteredText.Text = name;
            if (UserEnteredText.Text == "" || UserEnteredText.Text == null)
                UserEnteredText.Text = "Anonymous";
            UserEnteredText.Text += " picked " + veggie;
        }
	}
}