﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace hw3app1
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private void On_Veggie_Clicked(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            Navigation.PushAsync(new FavoriteVegetable(UserEntry.Text, button.Text));
        }
    }
}
